# README #

### What is this repository for? ###
 
* This Rest API is able to generate a multiple sequence alignment by using a gene symbol and the given species.
* A species, gene name and muscle parameters are entered into the Rest API.
* An Ensembl Rest API is used to translate the gene symbol into an Ensembl ID by using a species and gene symbol parameter.
* After this, the sequences of the Ensembl ID's are retrieved and a fasta file is built.
* A multiple sequence alignment is done on the fasta file.
* The user receives a JSON with the multiple sequence alignment.

### Usage ###

* Clone or fork this repository using git
* Run jar with java8 -jar muscle_ensembl_restapi-V1.2-0.0.1-SNAPSHOT.jar
* Endpoints:
* http://localhost:8080/api/align
* http://localhost:8080/api/translategenesymbol
* Example align endpoint:
* http://localhost:8080/api/align?species=human,mouse&symbol=mir140
* Required GET request parameters:
* species, symbol
* Optional GET request parameters (Default parameters are used if unavailable):
* email, title, format, tree, order, outfmt
* For information about muscle parameters read www.ebi.ac.uk/Tools/webservices/services/msa/muscle_rest
* Rest API documentation is available at http://localhost:8080/docs/index.html
* The health status of the REST APIs is available at http://localhost:8080/health in a JSON format

### Contributor ###

* Richard Harmanni BFV-3