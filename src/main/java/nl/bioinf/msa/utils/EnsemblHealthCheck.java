package nl.bioinf.msa.utils;

import nl.bioinf.msa.model.PingDTO;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * This component checks if the ensembl service is UP and adds some additional information about the service
 * The information gets added to the health endpoint.
 * Created by rharmanni on 12-6-17.
 */
@Component
public class EnsemblHealthCheck implements HealthIndicator {

    private RestTemplate template = new RestTemplate();

    /**
     * Check if the health of the ensembl service is ok, if service is up add info details.
     *
     * @return
     */
    @Override
    public Health health() {
        // perform health check
        int ping = check();
        if (ping == 0) {
            return Health.down().build();
        }
        // Return with extra details.
        return Health.up().withDetail("info", getInfo()).build();
    }

    /**
     * Check if the ensembl rest service is UP or DOWN.
     *
     * @return Ping status.
     */
    private int check() {
        final String pingURL = "https://rest.ensembl.org/info/ping?content-type=application/json";
        /*
        The response object is a direct mapping of the Ensembl response data using the `template.getForObject`
        call. This `RestTemplate` method calls the EnsemblServiceURL and maps it to a new PingDTO object.
         */
        PingDTO response = template.getForObject(pingURL, PingDTO.class);
        return response.getPing();

    }

    /**
     * Get extra information from the ensembl service, in this case only the version URL.
     *
     * @return Map with information.
     */
    private Map getInfo() {
        // Version ensembl url.
        final String versionURL = "https://rest.ensembl.org/info/rest?content-type=application/json";
        /*
        The response object is a direct mapping of the Ensembl response data using the `template.getForObject`
        call. This `RestTemplate` method calls the EnsemblServiceURL and maps it to a new Map object.
         */
        return template.getForObject(versionURL, Map.class);
    }

}
