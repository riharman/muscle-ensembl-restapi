package nl.bioinf.msa.utils;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * This component checks if the muscle service is up and adds the status to the health endpoint.
 * Created by rharmanni on 12-6-17.
 */
@Component
public class MuscleHealthCheck implements HealthIndicator {

    private RestTemplate template = new RestTemplate();

    /**
     * Check the health of the muscle service.
     *
     * @return
     */
    @Override
    public Health health() {
        // perform health check
        int ping = check();
        if (ping == 0) {
            return Health.down().build();
        }
        return Health.up().build();
    }

    /**
     * Check if the muscle service is available.
     *
     * @return health status
     */
    private int check() {
        String pingURL = "http://www.ebi.ac.uk/Tools/services/rest/muscle/";
        /*
        The response object is a direct mapping of the Ensembl response data using the `template.getForObject`
        call. This `RestTemplate` method calls the EnsemblServiceURL and maps it to a new String object.
         */
        ResponseEntity response = template.exchange(pingURL, HttpMethod.GET, null, String.class);
        // Check if the HttpStatus is OK.
        if (response.getStatusCode() == HttpStatus.OK) {
            return 1;
        }
        return 0;
    }

}
