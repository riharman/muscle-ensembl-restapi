package nl.bioinf.msa.control;

import nl.bioinf.msa.model.EnsemblIdDTO;
import nl.bioinf.msa.model.FastaDTO;
import nl.bioinf.msa.model.SequenceDTO;
import nl.bioinf.msa.service.EnsemblService;
import nl.bioinf.msa.service.FastaService;
import nl.bioinf.msa.service.MuscleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import java.net.URISyntaxException;
import java.util.*;

/**
 * Rest controller to retrieve the multiple sequence alignment of multiple species.
 * Created by rharmanni on 10-5-17.
 */
@RestController
@RequestMapping("/api")
public class AlignmentController {

    private EnsemblService ensemblService;

    private MuscleService muscleService;

    private FastaService fastaService;

    @Value("${muscle.email}")
    private String defaultEmail;

    @Value("${muscle.title}")
    private String defaultTitle;

    @Value("${muscle.format}")
    private String defaultFormat;

    @Value("${muscle.tree}")
    private String defaultTree;

    @Value("${muscle.order}")
    private String defaultOrder;

    @Value("${muscle.outfmt}")
    private String defaultOutfmt;

    @Autowired
    public AlignmentController(EnsemblService ensemblService, MuscleService muscleService, FastaService fastaService) {
        this.ensemblService = ensemblService;
        this.muscleService = muscleService;
        this.fastaService = fastaService;
    }

    /**
     * Process the client GET request. The gene symbols are translated into Ensembl ID's.
     * The sequences of the Ensembl ID's are retrieved using the Ensembl REST API's.
     * A fasta file is made from the sequences.
     * The muscle service runs muscle using the muscle REST API.
     * The multiple sequence alignment is retrieved and sent to the client.
     *
     * @param species The species name.
     * @param symbol  The gene symbol name.
     * @param email   Email field for EBI, must be a valid address
     * @param title   Title of the submission
     * @param format  Input file format, defaults to 'fasta'
     * @param tree    MUSCLE tree parameter, defaults to 'none'
     * @param order   MUSCLE order parameter, defaults to 'aligned'
     * @param outfmt  Format for the output, defaults to 'aln-fasta'
     * @return multiple sequence alignment
     */
    @RequestMapping(value = "/align", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity alignSpeciesByGeneSymbol(@RequestParam("species") String species,
                                                   @RequestParam("symbol") String symbol,
                                                   @RequestParam("email") Optional<String> email,
                                                   @RequestParam("title") Optional<String> title,
                                                   @RequestParam("format") Optional<String> format,
                                                   @RequestParam("tree") Optional<String> tree,
                                                   @RequestParam("order") Optional<String> order,
                                                   @RequestParam("outfmt") Optional<String> outfmt) {

        // Split the species at the comma into a list.
        List<String> speciesArray = Arrays.asList(species.split(","));

        // Loop over all species.
        // Create stringbuilder to build fasta file.
        StringBuilder fasta = new StringBuilder();
        for (String sp : speciesArray) {
            // Translate the gene symbol to Ensembl ID.
            ResponseEntity<List<EnsemblIdDTO>> ensemblIdDTOS = ensemblService.translateGeneSymbol(sp, symbol);
            // Retrieve the sequence. first element at the moment.
            SequenceDTO sequenceDTO = ensemblService.retrieveSequence(
                    ensemblIdDTOS.getBody().get(0).getEnsemblId(), sp);
            // Create fastafile
            fasta.append(String.format(">%s\n", sequenceDTO.getId()));
            fasta.append(sequenceDTO.getSequence()); // TODO: fix length
            fasta.append("\n");
        }

        // Run muscle.
        try {
            String fastaOutput = muscleService.runMuscle(
                    fasta.toString(),
                    email.orElse(defaultEmail),
                    title.orElse(defaultTitle),
                    format.orElse(defaultFormat),
                    tree.orElse(defaultTree),
                    order.orElse(defaultOrder),
                    outfmt.orElse(defaultOutfmt));
            // Check the muscle result..
            switch (fastaOutput) {
                case "ERROR":
                    return createErrorReponseEntity("Fasta sequences are too long");
                case "FAILURE":
                    return createErrorReponseEntity("Fasta sequences could not be processed");
                case "NOT_FOUND":
                    return createErrorReponseEntity("Some species or symbols could not be found");
                default:
                    // Parse fasta into a list of fasta objects.
                    List<FastaDTO> fastaDTOS = fastaService.parseFastaString(fastaOutput);
                    // Return the fasta map.
                    return new ResponseEntity(fastaDTOS, HttpStatus.OK);
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return createErrorReponseEntity(e.getMessage());
        }
    }

    /**
     * This method creates and returns a bad request response entity with an error message.
     *
     * @param message The error message
     * @return A response entity with an error message.
     */
    private ResponseEntity<Map<String, String>> createErrorReponseEntity(String message) {
        Map<String, String> error = new HashMap<>();
        error.put("error", message);
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }
}