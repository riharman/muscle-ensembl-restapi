package nl.bioinf.msa.control;

import nl.bioinf.msa.model.SequenceDTO;
import nl.bioinf.msa.service.EnsemblService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;


/**
 * This controller handles the gene symbol translations.
 * Created by rharmanni on 31-5-17.
 */
@RestController
@RequestMapping("/api")
public class GeneSymbolController {

    private EnsemblService ensemblService;

    @Autowired
    public GeneSymbolController(EnsemblService ensemblService) {
        this.ensemblService = ensemblService;
    }

    /**
     * Translates an external gene symbol into the ensembl gene symbols.
     *
     * @param species The species.
     * @param symbol  The gene symbol.
     * @return A response entity with the translated gene symbols and request status.
     */
    @RequestMapping(value = "/translategenesymbol", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity translateGeneSymbol(@RequestParam("species") String species,
                                              @RequestParam("symbol") String symbol) {
        return ensemblService.translateGeneSymbol(species, symbol);
    }

    /**
     * Retrieve a fasta sequence by using an ensembl ID.
     *
     * @param id      The ensembl id
     * @param species The species.
     * @return A response entity with the sequence and request status.
     */
    @RequestMapping(value = "/sequence", method = RequestMethod.GET, produces = "application/json")
    public SequenceDTO retrieveSequence(@RequestParam("id") String id,
                                        @RequestParam("species") Optional<String> species) {
        return ensemblService.retrieveSequence(id, species.orElse("not given"));
    }

}
