package nl.bioinf.msa.model;

/**
 * Created by rharmanni on 10-5-17.
 */
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by marcelk on 03/05/2017.
 *
 * Designed to hold Ensembl IDs
 *
 * Note: the `JsonProperty` annotation couples the actual JSON
 *       field name (in parenthesis) to the property name.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EnsemblIdDTO {

    @JsonProperty("type")
    public String featureType;

    @JsonProperty("id")
    public String ensemblId;

    public String getFeatureType() {
        return featureType;
    }

    public void setFeatureType(String featureType) {
        this.featureType = featureType;
    }

    public String getEnsemblId() {
        return ensemblId;
    }

    public void setEnsemblId(String ensemblId) {
        this.ensemblId = ensemblId;
    }

    @Override
    public String toString() {
        return "EnsemblIdDTO{" +
                "featureType='" + featureType + '\'' +
                ", ensemblId='" + ensemblId + '\'' +
                '}';
    }
}
