package nl.bioinf.msa.model;

/**
 * Holds information about ping.
 * Created by rharmanni on 12-6-17.
 */
public class PingDTO {

    int ping;

    public int getPing() {
        return ping;
    }

    public void setPing(int ping) {
        this.ping = ping;
    }
}
