package nl.bioinf.msa.model;

/**
 * The fasta object that holds the id and sequence.
 * Created by rharmanni on 23-6-17.
 */
public class FastaDTO {
    private String id;
    private String sequence;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }
}
