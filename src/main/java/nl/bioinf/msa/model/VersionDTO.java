package nl.bioinf.msa.model;

/**
 * Created by rharmanni on 12-6-17.
 */
public class VersionDTO {

    public String release;

    public String getRelease() {
        return release;
    }

    public void setRelease(String release) {
        this.release = release;
    }
}
