package nl.bioinf.msa.model;

/**
 * Created by rharmanni on 10-5-17.
 */
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by marcelk on 03/05/2017.
 *
 * Designed to hold Ensembl sequence data
 *
 * Note: the `JsonProperty` annotation couples the actual JSON
 *       field name (in parenthesis) to the property name.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SequenceDTO {

    @JsonProperty("desc")
    public String description;

    public String id;

    private String species;

    @JsonProperty("seq")
    public String sequence;

    public String molecule;

    public long length;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String seq) {
        this.sequence = seq;
    }

    public String getMolecule() {
        return molecule;
    }

    public void setMolecule(String molecule) {
        this.molecule = molecule;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public void calculateLength() {
        this.length = this.getSequence().length();
    }

}