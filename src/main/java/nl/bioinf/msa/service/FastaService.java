package nl.bioinf.msa.service;

import nl.bioinf.msa.model.FastaDTO;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * This service handles fasta files.
 * Created by rharmanni on 23-6-17.
 */
@Service
public class FastaService {

    /**
     * This method parses a fasta string and returns a list of fasta objects with the sequences and their information.
     * @param fasta The fasta string.
     * @return List with fasta data type objects.
     */
    public List<FastaDTO> parseFastaString(String fasta) {
        List<FastaDTO> fastaDTOS = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new StringReader(fasta));
        String line;
        String description = null;
        StringBuilder fastaSequence = new StringBuilder();
        try {
            // read line
            line = reader.readLine();
            while (line != null) {
                if (line.startsWith(">")) {
                    // parse description.
                    if (description != null) {
                        FastaDTO fastaDTO = new FastaDTO();
                        fastaDTO.setId(description);
                        fastaDTO.setSequence(fastaSequence.toString());
                        fastaDTOS.add(fastaDTO);
                    }
                    description = line.substring(1, line.length());
                    fastaSequence = new StringBuilder();
                } else {
                    // add sequence.
                    fastaSequence.append(line);
                }
                line = reader.readLine();
            }
            // Create new fasta data type object.
            FastaDTO fastaDTO = new FastaDTO();
            fastaDTO.setId(description);
            fastaDTO.setSequence(fastaSequence.toString());
            // Add to list.
            fastaDTOS.add(fastaDTO);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fastaDTOS;
    }

}
