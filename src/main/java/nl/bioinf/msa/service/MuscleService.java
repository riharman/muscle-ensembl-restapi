package nl.bioinf.msa.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Service that handles muscle REST API calls.
 * Created by rharmanni on 17-5-17.
 */
@Service
public class MuscleService {

    private RestTemplate template = new RestTemplate();

    @Value("${muscle.uri.run}")
    private String muscleRUN;

    @Value("${muscle.uri.status}")
    private String muscleSTATUS;

    @Value("${muscle.uri.result}")
    private String muscleRESULT;

    @Value("${muscle.status.interval}")
    private int interval;

    @Value("${muscle.status.maxwait}")
    private int maxwait;

    /**
     * Run the MUSCLE multiple-sequence-aligner on a given file
     *
     * Usage using curl:
     * Minimal:
     * curl --form "file=@sequences.fa" http://localhost:8080/ebimuscle/api/v1/runmuscle
     *
     * Complete:
     * curl --form 'email=your.name@domain.com' --form 'title=muscle_run' --form 'format=fasta' \
     * --form 'tree=none' --form 'order=aligned' --form 'outfmt=aln-fasta' \
     * --form "file=@sequences.fa"
     *
     * @param file   Input file, default fasta, change with 'format' parameter
     * @param email  Email field for EBI, must be a valid address
     * @param title  Title of the submission
     * @param format Input file format, defaults to 'fasta'
     * @param tree   MUSCLE tree parameter, defaults to 'none'
     * @param order  MUSCLE order parameter, defaults to 'aligned'
     * @param outfmt Format for the output, defaults to 'aln-fasta'
     * @return the alignment in the given output format (outfmt)
     * @throws URISyntaxException
     */
    public String runMuscle(String file,
                            String email,
                            String title,
                            String format,
                            String tree,
                            String order,
                            String outfmt
    ) throws URISyntaxException {

        /* Store uploaded file in a byte array */
        byte[] bytes = null;
        if (!file.isEmpty()) {
            try {
                bytes = file.getBytes();
            } catch (Exception e) {
                System.out.println("You failed to upload a file => " + e.getMessage());
            }
        } else {
            System.out.println("You failed to upload a file because the file was empty.");
        }

        System.out.println(email + " " + title + " " + format + " " + tree + " " + order + " " + outfmt);
        /* Store all parameters to POST in a LinkedMultiValueMap */
        MultiValueMap<String, String> mvm = new LinkedMultiValueMap<>();
        mvm.add("email", email);
        mvm.add("title", title);
        mvm.add("format", format);
        mvm.add("tree", tree);
        mvm.add("order", order);
        // Convert the contents of the byte array to a String
        mvm.add("sequence", new String(bytes));

        /* POST the RUN request to EBI with all data, and receive a String (jobId) */
        // TODO: properly check if a job ID has been received
        String jobId = template.postForObject(new URI(muscleRUN), mvm, String.class);
        System.out.println("Job has been submitted and has the following ID: " + jobId);

        /* Poll the status of the job and act accordingly */
        int iterations = maxwait / interval;
        for (int i = 0; i < iterations; i++) {
            try {
                System.out.println("@Interval " + i);
                /* Sleep for the configured interval before requesting a new status update */
                Thread.sleep(interval);

                /* GET the status for the submitted job */
                // TODO: properly check if a status message has been received
                String jobStatus = template.getForObject(new URI(muscleSTATUS + jobId), String.class);

                /* Depending on the jobStatus, return a String or keep polling */
                switch (jobStatus) {
                    case "FINISHED":
                        // TODO: properly check if an alignment has been received
                        String resultURI = muscleRESULT + jobId + "/" + outfmt;
                        System.out.println("\tStatus: FINISHED, fetching result");
                        return template.getForObject(new URI(resultURI), String.class);
                    case "ERROR":
                        return "ERROR";
                    case "FAILURE":
                        return "FAILURE";
                    case "NOT_FOUND":
                        return "NOT_FOUND"; //Status: NOT_FOUND
                    case "RUNNING":
                        System.out.println("\tStatus: RUNNING.");
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        return "ERROR, should not have reached this!";
    }

}