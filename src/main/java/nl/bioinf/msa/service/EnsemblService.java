package nl.bioinf.msa.service;

import nl.bioinf.msa.model.EnsemblIdDTO;
import nl.bioinf.msa.model.SequenceDTO;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service that handles ensembl REST API calls.
 * Created by rharmanni on 10-5-17.
 */
@Service
public class EnsemblService {

    private RestTemplate template = new RestTemplate();

    public ResponseEntity<List<EnsemblIdDTO>> translateGeneSymbol(String species, String symbol) {

        // Ensembl REST API URL.
        // Note: use application.properties for storing static URL part and content type
        String EnsemblServiceURL =
                String.format("https://rest.ensembl.org/xrefs/symbol/%s/%s?content-type=application/json",
                        species, symbol);

        // This object is just a type reference to tell the `template.exchange()` method that
        // we want the received data stored in a List of EnsemblIdDTO objects.
        ParameterizedTypeReference<List<EnsemblIdDTO>> listOfEnsemblIdDTOs =
                new ParameterizedTypeReference<List<EnsemblIdDTO>>() {
                };
        /*
            Retrieves the JSON data from Ensembl (given the EnsemblServiceURL) and maps this to one
            or more EnsemblIdDTO objects.
            Arguments to template.exchange are as follows:
                - REST URL for retrieving JSON input data.
                - HTTP method for retrieving the data (GET in this case).
                - An optional `requestEntity` (extension of `HttpEntity`) that represents a HTTP request or
                  response containing both headers and body.
                - The type reference we want the data to be stored in.
         */
        try {
            ResponseEntity<List<EnsemblIdDTO>> ensemblResponse = template.exchange(EnsemblServiceURL,
                    HttpMethod.GET, null, listOfEnsemblIdDTOs);
            return ensemblResponse;
        } catch (HttpClientErrorException e) {
            return new ResponseEntity(e.getResponseBodyAsString(), HttpStatus.BAD_REQUEST);
        }
        // 'copy' the actual list of objects for return, stored as body in the `ensemblResponse`

    }

    public SequenceDTO retrieveSequence(String id, String species) {

        // Ensembl REST API URL.
        // Note: use application.properties for storing static URL part and content type
        String EnsemblServiceURL =
                String.format("https://rest.ensembl.org/sequence/id/%s?content-type=application/json", id);
        /*
        The response object is a direct mapping of the Ensembl response data using the `template.getForObject`
        call. This `RestTemplate` method calls the EnsemblServiceURL and maps it to a new SequenceDTO object.
         */
        SequenceDTO response = template.getForObject(EnsemblServiceURL, SequenceDTO.class);
        // Add some sequence statistic to the object manually
        response.calculateLength();
        // Set the species to the object.
        response.setSpecies(species);

        // Return SequenceDTO object, Spring will translate this to a JSON response
        return response;
    }

}
