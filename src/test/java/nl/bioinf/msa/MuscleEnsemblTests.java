package nl.bioinf.msa;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.restdocs.RestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by rharmanni on 31-5-17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
public class MuscleEnsemblTests {

    @Rule
    public final RestDocumentation restDocumentation = new RestDocumentation("build/generated-snippets");

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private MockMvc mockMvc;

    private RestDocumentationResultHandler document;

    @Before
    public void setUp() {
        this.document = document("{method-name}",
                preprocessRequest(prettyPrint()),
                preprocessResponse(prettyPrint()));

        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                .apply(documentationConfiguration(this.restDocumentation))
                .alwaysDo(this.document)
                .build();
    }

    /**
     * Test the gene symbol translation endpoint "/api/translategenesymbol" and add documentation using mockMvc.
     *
     * @throws Exception
     */
    @Test
    public void translateGeneSymbol() throws Exception {
        String responseJson = "[{'type':'gene','id':'ENSG00000204397'},{'type':'gene','id':'ENSG00000143207'}]";


        // Perform a GET request for the human BRCA2 gene.
        this.mockMvc.perform(get("/api/translategenesymbol?species={species}&symbol={genesymbol}",
                "human", "COP1"))
                .andExpect(status().isOk())
                .andExpect(content().json(responseJson))
                .andDo(document("{method-name}",
                        responseFields(
                                fieldWithPath("[].type").description("The ensembl type."),
                                fieldWithPath("[].id").description("The ensembl ID.")
                        ),
                        requestParameters(
                                parameterWithName("species").description("The species."),
                                parameterWithName("symbol").description("The gene symbol to be translated.")
                        )
                        )
                );
    }

    /**
     * Test align endpoint "/api/translategenesymbol" and add documentation using mockMvc.
     *
     * @throws Exception
     */
    @Test
    public void alignSpeciesByGeneSymbol() throws Exception {
        String responseJson = "[{\"id\":\"ENSG00000208017\",\"sequence\":\"TGTGTCTCTCTCTGTGTCCTGCCAGTGGTTTTACCCTATGGT" +
                "AGGTTACGTCATGCTGTTCTACCACAGGGTAGAACCACGGACAGGATACCGGGGCACC\"},{\"id\":\"ENSMUSG00000065439\",\"seque" +
                "nce\":\"-----------------CCTGCCAGTGGTTTTACCCTATGGTAGGTTACGTCATGCTGTTCTACCACAGGGTAGAACCACGGACAGG-----" +
                "--------\"}]";

        // Perform a GET request for the human and mouse MIR140 gene.
        this.mockMvc.perform(get("/api/align?species={species}&symbol={genesymbol}",
                "human,mouse", "mir140"))
                .andExpect(status().isOk())
                .andExpect(content().json(responseJson))
                .andDo(document("{method-name}",
                        responseFields(
                                fieldWithPath("[].id").description("The ensembl ID."),
                                fieldWithPath("[].sequence").description("The aligned sequence.")
                        ),
                        requestParameters(
                                parameterWithName("species").description(
                                        "The species (multiple species comma separated)."),
                                parameterWithName("symbol").description("The gene symbol to be translated.")
                        )
                        )
                );

    }

}
